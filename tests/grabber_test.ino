/*
    grabber_test - take a wild guess what it does
*/

#include "platform.h"

bool grabber_active = false;

void setup() {
    setup_platform();
    Serial.begin(9600);
}

void loop() {
    if (grabber_active){
            grabber_active = false;
            Serial.println("off");             
            grabber_release();
        }
    else{
        grabber_grab();
        grabber_active = true;
        Serial.println("on");
        }
    
    delay(1000);
}
