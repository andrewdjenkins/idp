#include "platform.h"

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

unsigned long start_time;

bool correct_start_orientation = false;

void setup()
{
    Serial.begin(9600);
    start_time = millis();

    setup_platform();
}

// void turn_right(int degrees)
// {
//     //rotating clockwise
//     run_motor(255 , 20);
//     int time1 = floor(degrees * 40 / (4 * 360));
//     delay(2 * time1 * 1000);
// }

void turn_right_two_wheels(int degrees)
{
    //rotating clockwise
    unsigned long int start_time1 = millis();
    int time1 = floor(degrees * 8 / (2 * 360))*1000;
    while (millis() - start_time1 < time1)
    {
        run_motor(-255, 255);
    }
}

 void forward(int distance)
 {
  //distance in cm, time in ms
     unsigned long int start_time0 = millis();
     int time0 = floor(distance * 10 / 200);
     while (millis() - start_time0 < time0*1000)
     {
         run_motor(-255, -255);
     }
 }



inline void line_follow_basic()
{

    if (line_white('R')  && !line_white('L'))
    {

        run_motor(-220, 0);
        delay(20);
    }

    else if (!line_white('R'))
    {
        run_motor(-60, -120);
        delay(10);
    }
    else if (line_white('R') && line_white('L') && (millis() - start_time) >= 10000)
    {
        forward(25);
        turn_right_two_wheels(90);
       
        correct_start_orientation = true;
        Serial.println(correct_start_orientation);
    }
}

void loop()
{
  Serial.println("START");
    
    Serial.println(correct_start_orientation);
    if (correct_start_orientation)
        {
            run_motor(0, 0);
            Serial.println("AT 0");
        }
    else{
      //  start_button(line_follow_basic);
        line_follow_basic();
        
        
    }

}


