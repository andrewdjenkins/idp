#!/bin/sh

[ ! -z "$1" ] && {
arduino-cli compile -b arduino:megaavr:uno2018 $1 &&
arduino-cli upload --verify -p /dev/ttyACM0 -b arduino:megaavr:uno2018 $1
} || echo "Must specify sketch name"
