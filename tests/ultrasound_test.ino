/* 
    ultrasound.ino - ADJ 2020 for IDP
    based on datasheet at:
    https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf
*/
#define US_TRIGGER_PIN 4
#define US_ECHO_PIN 5
#define US_INVALID -1

//TODO? Might want to make sure it can't be called more than once per reasonable
// time interval...

void setup_ultrasound () {
    pinMode(US_ECHO_PIN, INPUT);
    pinMode(US_TRIGGER_PIN, OUTPUT);
}

int ultrasound_dt () {
    // returns the time in microseconds between the pulse being sent and its
    // echo being recieved.
    int send_time = micros();   
    // raise voltage on pin for 10 us
    digitalWrite(US_TRIGGER_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(US_TRIGGER_PIN, LOW);
    
    int dt;
    int high_start; // what time does the high pulse start?
    bool high_started = false;

    // 2 * 5 metres / 340 ms-1 = 29 ms so wait 30 ms before saying sod it
    do {
        dt = micros() - send_time;
        if ((!high_started) && (digitalRead(US_ECHO_PIN) == HIGH)) {
            // start timing when the echo pin rises
            high_start = micros();
            high_started = true;
        }
	if (high_started && (digitalRead(US_ECHO_PIN) == LOW)) {
            // stop timing when it falls again.
            return micros() - high_start;
        }
    } while (dt < 35000);
    
    // if the pulse didn't get back then bin it.
    return (US_INVALID);
}

float ultrasound_range () {
    // returns current range in centimetres
    int dt = ultrasound_dt();
    return (dt == US_INVALID) ? US_INVALID : (float) dt / 58;
}

// this is the test-specific stuff.

void setup() {
    setup_ultrasound();
    Serial.begin(9600);
}

void loop() {
    Serial.println(ultrasound_range());
    delay(500);
}

