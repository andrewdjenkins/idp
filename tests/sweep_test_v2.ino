/*
    motion_test.ino
    A test script for controlled free motion.
*/

#include "field_motion.h"
#include "platform.h"

#define US_TRIGGER_PIN 4
#define US_ECHO_PIN 5
#define US_INVALID -1

int amplitudePin = A2; //  The analogue pin that the signal pin of the sensor is connected to
int typePin5 = 1;
int typePin3 = 2;
int IRMax = 20;
float sensorValue;

void setup()
{
    Serial.begin(9600);
    setup_platform();
    setup_field_motion();
}

// void forward(int distance)
// {
//     int start_time = millis();
//     int time0 = floor(distance * 10 / 200);
//     while (millis() - start_time < time0)
//     {
//         run_motor(255, 255);
//     }
// }

// void turn_right_one_wheel(int degrees)
// {
//     //rotating clockwise
//     int start_time1 = millis();
//     int time1 = floor(degrees * 31 / (2 * 360));
//     while (millis() - start_time1 < time1)
//     {
//         run_motor(255, 0);
//     }
// }

// void turn_left_one_wheel(int degrees)
// {
//     //rotating anticlockwise
//     int start_time2 = millis();
//     int time2 = floor(degrees * 31 / (2 * 360));
//     while (millis() - start_time2 < time2)
//     {
//         run_motor(0, 255);
//     }
// }

void loop()
{
    int start_sweep = millis();
    //values for motor at full speed (255)
    int time_sweep = floor(180 * 31 / (2 * 360));
    while (millis() - start_sweep < 2 * time_sweep)
    {
        run_motor(255 / 2, 0)
            // if it's still in progress search for robots
            sensorValue = int(analogRead(amplitudePin));
        //Serial.println(sensorValue);

        if (sensorValue <= IRMax)
        {
            // stop now
            run_motor(0, 0);

            Serial.println("Pointing at the robot");
            Serial.println("this is the distance to the robot:");
            Serial.println(ultrasound_range());
        }
    }
    run_motor(0, 0);
}
