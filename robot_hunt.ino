#include "field_motion.h"
#include "platform.h"

void setup ()
{
    setup_field_motion();
    setup_platform();
}

bool done = false;

void main ()
{
    if (!done) {
        done = (robot_hunt() == RH_DONE);
    }
}
