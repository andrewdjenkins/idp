#include "platform.h"

inline void line_follow_basic()
{
    // Blindly follows the line until both sensors read 'white', or until some
    // time limit is reached.
    unsigned long start_time = millis();
    // variables

    //while (millis() - start_time < TIMEOUT_MILLIS)
    // assumes for the moment that the sensors are ahead of the wheels.

    if (line_white('L'))
    {
        if (line_white('R'))
        {
            // this is the case if both are on the line.
            // in this simple test it will just stop.
            run_motor(0, 0);
            delay(10);
            //break;
        }
        else
        {
            // in this case the robot is too far to the right so speed up
            // the RH motor
            run_motor(200 0);
            delay(10);
        }
    }
    else
    {
        if (line_white('R'))
        {
            // in this case the robot is too far to the left so speed up
            // the LH motor
            run_motor(0, 200);
            delay(10);
        }
        else
        {
            // just keep going in a straight line
            run_motor(200, 200);
            delay(10);
        }
    }
}

void setup()
{
    Serial.begin(9600);
    pinMode(ledPin, OUTPUT);
    setup_platform();
}

void loop()
{
    uint8_t i;

    line_follow_basic();
}
