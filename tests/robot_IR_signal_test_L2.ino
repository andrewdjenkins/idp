/*
 * IRremote: IRsendRawDemo - demonstrates sending IR codes with sendRaw
 * An IR LED must be connected to Arduino PWM pin 3.
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 *
 * IRsendRawDemo - added by AnalysIR (via www.AnalysIR.com), 24 August 2015
 *
 * This example shows how to send a RAW signal using the IRremote library.
 * The example signal is actually a 32 bit NEC signal.
 * Remote Control button: LGTV Power On/Off. 
 * Hex Value: 0x20DF10EF, 32 bits
 * 
 * It is more efficient to use the sendNEC function to send NEC signals. 
 * Use of sendRaw here, serves only as an example of using the function.
 * 
 */

#include <IRremote.h>
#include <IRremoteInt.h>
#include <timer.h>
/******************
 * Modify line below according to board in use:
 * Nano Every (robots) - pin 6
 * Orange Pip Kona 328 - pin 3
 * Orange Pip Mega2560 - pin 9
 ******************/
#define TIMER_PWM_PIN 3 //defined in the library PIN 3 for orange pip, 6 for nano every.
IRsend irsend;
auto timer = timer_create_default();
int LEDpin = 13;
void setup()
{

  pinMode(TIMER_PWM_PIN, OUTPUT);
  pinMode(LEDpin, OUTPUT);
  digitalWrite(TIMER_PWM_PIN, LOW);
  int khz = 38; // 38kHz carrier frequency for the NEC protocol
  IRsend myIR;
  timer.every(100, sendSig5);

  myIR.enableIROut(khz);
  // TIMER_ENABLE_PWM;
}

void sendSig5()
{
  int i;
  digitalWrite(LEDpin, HIGH);
  //change for loop max value to 3 for 'charging' test signal
  //change for loop max value to 5 for 'service' test signal
  for (i = 0; i < 5; i++)
  {
    TIMER_ENABLE_PWM;
    delayMicroseconds(600);
    TIMER_DISABLE_PWM;
    delayMicroseconds(600);
  }
  digitalWrite(LEDpin, LOW);
}

void loop()
{
  timer.tick();
}
