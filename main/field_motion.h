/*
    field_motion.h
    Declarations for motion control in 'field mode'
*/  

#ifndef FIELD_MOTION_H_
#define FIELD_MOTION_H_

// physical platform parameters
#define WHEELSPAN_CM 24.3909

#define DEGREES_PER_RADIAN 57.2958 

// control parameters
#define NOMINAL_SPEED 50
// i.e. speed up / slow down 10% to keep on track.
#define CORRECTION_RATIO 1.1
#define TARGET_GRAB_RANGE_CM 5 // how close should we get before grabbing?

// Fallback time-based control.

// milliseconds to move forward a centimetre at full speed
#define MILLIS_PER_CM 153.35
// milliseconds to rotate through one degree with left wheel only
#define MILLIS_PER_DEGREE_LEFT 22.6736
// milliseconds to rotate through one degree with right wheel only
#define MILLIS_PER_DEGREE_RIGHT 21.76

typedef enum MotionType {
    M_STOPPED,
    // these are for turning with both wheels at different speeds.
    M_LEFT,
    M_RIGHT,
    // these are for ostensibly straight motion
    M_FORWARD,
    M_BACKWARD,
    // these are for single-encoder operation: rotation with LH wheel stationary
    M_L_PIVOT_LEFT,
    M_L_PIVOT_RIGHT,
} MotionType;

typedef enum MoveActionResult {
    MA_INPROGRESS, // returned if the action was not completed this time
    MA_COMPLETE, // returned if the conditions of the action were satisfied.
    MA_ERROR, // something went wrong
} MoveActionResult;

// what can be returned by look_for_robot()
typedef enum RobotHuntResult {
    RH_INPROGRESS, // call this again next loop
    RH_DROPOFF, // switch to line follow please!
    RH_DONE, // no more robots found
} RobotHuntResult;

// X, Y, Theta
typedef struct XYT {
    float theta;
    float x;
    float y;
} XYT;

// an move action encapsulates a single phase of movement.
typedef struct MoveAction {
    MotionType type;
    /* 
        distance:
            Ignored for M_*_PIVOT_* motion type
            Forward cm for M_FORWARD or M_BACKWARD
            Central arc length cm for M_LEFT, M_RIGHT
    
        angle:
            Angle rotated in DEGREES for arc, pivot.
    */
    float distance;
    float angle;
} MoveAction;

/* 
    State variables.
    X and Y are coordinate axes such that +ve y is 90 anticlockwise of +ve x.
    angle is the angle the robot's axle makes with the -ve y axis. 
        (i.e. to an axis _|_ to x)
    Only really meaningful once an origin has been defined.
*/
extern float angle;
extern float x;
extern float y;

// Sets up variables etc necessary for motion in the field.
void setup_field_motion(void);

/* 
    Called in loop() to pursue the move action. It uses call-by-reference
    so for some action 'a', call 'do_move_action(&a)'.
    Each call will return:
        MA_INPROGRESS if the action is not yet complete (i.e. not yet at dest.)
            If this is returned, it should be called again with the same action
            next iteration of loop().
        MA_COMPLETE if the action was complete this call.
            If this is returned, it can be called with the next action.
        MA_ERROR if any error occurred.
            If this is returned, there was a case I haven't programmed yet.
*/
MoveActionResult do_move_action (MoveAction* action);

/* 
    Called in loop() to approach and grab a robot. Returns:
        MA_INPROGRESS if Ricardo is still in the field doing stuff.
            If this is returned, it should be called again iteration of loop().
        MA_COMPLETE when Ricardo thinks he has grabbed the robot and is totally
            done. When this is returned, the position state variables are updated.
        MA_ERROR if Ricardo thinks he's gone off-course. He doesn't know which
            way this is likely to be, but it is likely that if we swept left to
            right, we will be too far left, and vice versa.
*/
MoveActionResult approach_and_grab ();

/* 
    Called in loop() for a robot hunt.
        RH_INPROGRESS if Ricardo is still in the field doing stuff.
            If this is returned, it should be called again iteration of loop().
        RH_DONE if there are no robots left and Ricardo is on the white line
            ready to go home.
        RH_DROPOFF if Ricardo has retrieved a robot and is on the white line
            ready to go to dropoff.
*/
RobotHuntResult look_for_robot (void);

// increments the position state variables given the position change vector d_pos
void inc_position(XYT d_pos);

/*
    set the origin here and start measuring the position from this point.
    resets both the last_count variables and the position, so should only
    be called once(ish) from a known position!
*/
void set_origin (void);

// fully general change in position based on the two counts and the motion state
// over that time.
XYT pos_change(float angle, int n_left, int n_right, MotionType motion_state);

// update the position state variables based on a position change vector. Also
// resets the last_count variables. 
void update_position(XYT d_pos);

/*
    Update the position state variables based on motion of a single type 
    occuring since the last update of the position state variables.
    
    Call this, for example, after cancelling a MoveAction.
*/
void update_position_given_motion_type(MotionType motiontype);

/*
    Returns the change in position and rotation of the centre of the axle based 
    on an advance where both wheels move straight ahead, and the wheel rotates n
    counts
*/ 
XYT forward_dpos(int theta_0, int n_right);

/*
    return the change in position and rotation of the centre of the axle based 
    on a rotation where the left wheel is fixed and the right wheel rotates n 
    counts.
*/
XYT l_pivot_dpos(float theta_0, int n_right);

#endif /* FIELD_MOTION_H_ */
