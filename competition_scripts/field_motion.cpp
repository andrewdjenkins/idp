///*
//    field_motion.cpp
//
//    implements field_motion.h - controls for motion in the 'field' phase
//    of operation.
//*/
//
//#include "field_motion.h"
//#include "platform.h"
//#include <Arduino.h>
//
//// to be set by the motor control routines based on rel. motion of wheels.
//MotionType motion_state;
//
//// where angle is the angle relative to the y axis. (i.e. to an axis _|_ to x)
//float angle = 0;
//float x = 0;
//float y = 0;
//int current_instructions;
//
//// what the counts were last time we updated the position. (usually since the
//// start of the latest action)
//int left_count_last = 0;
//int right_count_last = 0;
//
//// the action we're working on at the moment.
//MoveAction current_action;
//
//// increment the position state variables based on the position change vector
//void inc_position(XYT d_pos) {
//    angle += d_pos.theta;
//    x += d_pos.x;
//    y += d_pos.y;
//}
//
//// set the origin here and start measuring the position from this point.
//// resets both the last_count variables and the position, so should only
//// be called once from a known position!
//void set_origin() {
//    left_count_last = left_count;
//    right_count_last = right_count;
//    x = 0;
//    y = 0;
//    angle = 0;
//}
//
//// return the change in position and rotation of the centre of the axle based on
//// an advance where both wheels move straight ahead, and the wheel rotates n
//// counts
//XYT forward_dpos(float theta_0, int n_right)
//{
//    // arc length traced by rh wheel
//    float ds = (n_right * DEGREES_PER_INTERRUPT / 360)
//                * WHEEL_CIRCUMFERENCE_CM;
//
//    float dy = ds * sin (theta_0);
//    float dx = ds * cos (theta_0);
//    XYT p; // encapsulate all of the deltas into a struct
//
//    p.theta = 0;
//    p.x = dx;
//    p.y = dy;
//
//    // this can be used to update the position state variables
//    return p;
//}
//
//// return the change in position and rotation of the centre of the axle based
//// on a rotation where the left wheel is fixed and the right wheel rotates n
//// counts.
//XYT l_pivot_dpos(float theta_0, int n_right)
//{
//    // arc length traced by rh wheel rel to lh wheel
//    float ds = (n_right * DEGREES_PER_INTERRUPT / 360)
//                * WHEEL_CIRCUMFERENCE_CM;
//
//    // change in abs angle of robot
//    float d_theta = DEGREES_PER_RADIAN * (ds / WHEELSPAN_CM);
//    float avg_theta = theta_0 + 0.5 * d_theta;
//    float dy = ds * sin (avg_theta);
//    float dx = ds * cos (avg_theta);
//    XYT p; // encapsulate all of the deltas into a struct
//
//    p.theta = d_theta;
//    p.x = dx;
//    p.y = dy;
//
//    // this can be used to update the position state variables
//    return p;
//}
//
//// good for predicting motion.
//// TODO: do some maths! This uses small angles
//XYT l_pivot_dpos_from_d_theta(float theta_0, float d_theta)
//{
//    // arc length traced by rh wheel rel to lh wheel = r_dtheta
//    float ds = WHEELSPAN_CM * d_theta / DEGREES_PER_RADIAN;
//
//    // change in abs angle of robot
//    float avg_theta = theta_0 + 0.5 * d_theta;
//    float dy = ds * sin (avg_theta);
//    float dx = ds * cos (avg_theta);
//    XYT p; // encapsulate all of the deltas into a struct
//
//    p.theta = d_theta;
//    p.x = dx;
//    p.y = dy;
//
//    // this can be used to update the position state variables
//    return p;
//}
//
//// fully general change in position based on the two counts and the motion state
//// over that time.
//XYT pos_change(float angle, int n_left, int n_right, MotionType motion_state) {
//    XYT dp;
//
//    switch(motion_state)
//    {
//    case M_FORWARD:
//        return forward_dpos(angle, n_left);
//
//    case M_L_PIVOT_LEFT:
//        return l_pivot_dpos(angle, n_right);
//
//    case M_L_PIVOT_RIGHT:
//        return l_pivot_dpos(angle, -n_right);
//    }
//
//    return dp;
//}
//
//// update the position state variables based on a given change in position.
//void update_position(XYT d_pos) {
//    inc_position(d_pos);
//    right_count_last = right_count;
//    left_count_last = left_count;
//}
//
//// update the position state variables given that we have been moving with type
//// 'type' since they were last updated.
//void update_position_given_motion_type( MotionType type ) {
//    int dl = left_count - left_count_last;
//    int dr = right_count - right_count_last;
//    update_position( pos_change(angle, dl, dr, type));
//}
//
///*
//    ACTUALLY PHYSICALLY MOVING
//*/
//
//// will try to proceed until the action is complete.
//MoveActionResult do_move_action (MoveAction *action) {
//    // todo: static MoveAction *last_action = NULL;
//
//    // the number of counts on either side that we want. Doesn't matter that
//    // it's a float since it's only used for comparison.
//    float tgt_l, tgt_r;
//    // the number of counts on either side that we've had since last action.
//    int dl = left_count - left_count_last;
//    int dr = right_count - right_count_last;
//
//    // 'central count', used only for arcs.
//    int c;
//
//    switch (action->type)
//    {
//        case M_FORWARD:
//        {
//            // distance = number of counts * (degrees per count /360) * circumference
//            tgt_l = 360 * (action->distance) / (WHEEL_CIRCUMFERENCE_CM * DEGREES_PER_INTERRUPT);
//            tgt_r = tgt_l;
//            // first check if we're there or have slightly overshot - if we are, stop.
//            if ((dl >= tgt_l) || (dr >= tgt_r)) {
//                run_motor(0, 0);
//                update_position(forward_dpos(angle, dr));
//                return MA_COMPLETE;
//            }
//            // otherwise check if we're pointing in the right direction.
//            else if ((dl - dr) < -1) {
//                // i.e. if right is too fast, speed up left wheel.
//                run_motor (NOMINAL_SPEED, NOMINAL_SPEED * CORRECTION_RATIO);
//            }
//            else if ((dl - dr) > 1) {
//                // i.e. if left is too fast, speed up right wheel.
//                run_motor (NOMINAL_SPEED * CORRECTION_RATIO, NOMINAL_SPEED);
//            } else {
//                run_motor (NOMINAL_SPEED, NOMINAL_SPEED);
//            }
//            return MA_INPROGRESS;
//        }
//        case M_L_PIVOT_LEFT:
//        {
//            // angle * wheelspan = number of counts * (degrees per count /360) * circumference
//            tgt_r = action->angle * 360 * WHEELSPAN_CM/ (WHEEL_CIRCUMFERENCE_CM * DEGREES_PER_INTERRUPT);
//            // first check if we're there or have slightly overshot - if we are, stop.
//            if ((dl >= tgt_l) || (dr >= tgt_r)) {
//                run_motor(0, 0);
//                update_position(l_pivot_dpos(angle, dr));
//                return MA_COMPLETE;
//            }
//            // otherwise keep going.
//            else if ((dl - dr) < -1) {
//                // i.e. if right is too fast, speed up left wheel.
//                run_motor (NOMINAL_SPEED, 0);
//                return MA_INPROGRESS;
//            }
//        }
//        case M_L_PIVOT_RIGHT:
//        {
//            // angle * wheelspan = number of counts * (degrees per count /360) * circumference
//            tgt_r = (action->angle) * 360 * WHEELSPAN_CM/ (WHEEL_CIRCUMFERENCE_CM * DEGREES_PER_INTERRUPT);
//            // first check if we're there or have slightly overshot - if we are, stop.
//            if ((dl >= tgt_l) || (dr >= tgt_r)) {
//                run_motor(0, 0);
//                update_position(l_pivot_dpos(angle, -dr));
//                return MA_COMPLETE;
//            }
//            // otherwise keep going.
//            else if ((dl - dr) < -1) {
//                run_motor (-NOMINAL_SPEED, 0);
//                return MA_INPROGRESS;
//            }
//        }
//        case M_LEFT:
//        {
//            // where R is radius of curvature = (distance / angle)
//            // outer distance = centre_distance * (1 + (wheelspan/2R))
//            // inner distance = centre_distance * (1 - (wheelspan/2R))
//            // and therefore likewise for counts.
//
//            // TODO MON 2nd: THIS.
//
//            // equivalent of 'central counts'
//            int c = 360* (action->distance) / (WHEEL_CIRCUMFERENCE_CM * DEGREES_PER_INTERRUPT);
//            tgt_r = tgt_l;
//
//            // because not finished
//            return MA_ERROR;
//        }
//        default:
//            // because not finished.
//            return MA_ERROR;
//
//    }
//}
//
///* CODE FOR GOING ON A ROBOT HUNT */
//
//typedef enum RobotHuntStage {
//    HS_PRE_SWEEP,
//    // searching for robots in need of repair
//    HS_S_REPAIR,
//    HS_S_RECHARGE,
//    // once a robot is found, this.
//    HS_APPROACH,
//    HS_RETREIVE,
//    // now go home.
//    HS_HOMETIME,
//} RobotHuntStage;
//
//static MoveAction turn_pre_sweep;
//static MoveAction advance_10;
//static MoveAction turn_180;
//static MoveAction sweep_l_to_r;
//static MoveAction sweep_r_to_l;
//static MoveAction* i_ptr = &turn_pre_sweep;
//
//void setup_field_motion() {
//    turn_pre_sweep.type = M_L_PIVOT_LEFT;
//    turn_pre_sweep.angle = 135;
//    sweep_l_to_r.type = M_L_PIVOT_RIGHT;
//    sweep_l_to_r.angle = 270;
//    sweep_r_to_l.type = M_L_PIVOT_LEFT;
//    sweep_r_to_l.angle = 270;
//
//}
//
//// how far away was the robot last time we looked?
//float last_range = 9999;
//
//// to be called in loop.
//MoveActionResult approach_and_grab(void) {
//    float current_range = ultrasound_range();
//    if (current_range > TARGET_GRAB_RANGE_CM) {
//        if (current_range > last_range) {
//            // are we getting further away? we shouldn't be...
//            // this would mean we were probably off course.
//            return MA_ERROR;
//        }
//        run_motor(NOMINAL_SPEED, NOMINAL_SPEED);
//        last_range = current_range;
//        return MA_INPROGRESS;
//    }
//    else { // if we're there then grab.
//        run_motor(0, 0);
//        grabber_grab();
//        last_range = 9999;
//        update_position_given_motion_type(M_FORWARD);
//        return MA_COMPLETE;
//    }
//}
//
///*
//    Given a buffer, will dump in some actions to get back to tunnel from current
//    position.
//
//    The basic scheme is:
//        - rotate
//        - proceed to (near) centre line (i.e. y = 0)
//        - rotate to -180 degrees
//
//    Returns number of actions.
//*/
//int actions_to_return_to_tunnel (MoveAction* actions) {
//
//    // point back at centre line
//    float target_angle = (y>0)? -90 : 90;
//
//    // angle we need to turn through first of all
//    float theta = target_angle - angle;
//
//    // +ve angle means turning left so first rotation should be to go left
//    if (theta > 0) {
//        actions[0].type = M_L_PIVOT_LEFT;
//        actions[0].angle = theta;
//    }
//    else {
//        actions[0].type = M_L_PIVOT_RIGHT;
//        actions[0].angle = -theta;
//    }
//
//    // now we need to work out what our y pos will be.
//    XYT dp1 = l_pivot_dpos_from_d_theta(angle, theta);
//    // our x and y when theta is set to 90. If we're at
//    float y1 = y + dp1.y;
//    float x1 = x + dp1.x;
//    float a1 = target_angle;
//
//    // so we can left pivot to reach zero angle in the middle
//    float target_y = - 0.5 * WHEELSPAN_CM;
//
//    float dy = target_y - y1;
//
//    // RHS (a1 > 0) and dy > 0 : forward
//    // LHS (a1 < 0) and dy < 0 : forward
//
//    // TODO: make sure this is right!
//    if ((dy > 0) xor (a1 < 0)) {
//        actions[1].type = M_BACKWARD;
//        actions[1].distance = abs(dy);
//    }
//    else {
//        actions[1].type = M_FORWARD;
//        actions[1].distance = abs(dy);
//    }
//
//    // left pivot to -180.
//    theta = -180 - a1;
//    if (theta > 0) {
//        actions[2].type = M_L_PIVOT_LEFT;
//        actions[2].angle = theta;
//    }
//    else {
//        actions[2].type = M_L_PIVOT_RIGHT;
//        actions[2].angle = -theta;
//    }
//
//    // finally advance to zero x plus 10cm to make sure in tunnel
//    actions[3].type = M_FORWARD;
//    actions[3].distance = x + 10;
//
//    // 4 instructions.
//    return 4;
//}
//
//// The loop() function.
//RobotHuntResult look_for_robot() {
//    // did we last do a sweep for robots needing charging, or for robots needing
//    // repair? (and, crucially, was this from the right or from the left?)
//    static RobotHuntStage last_sweep;
//    // what are we doing at the moment?
//    static RobotHuntStage current_stage = HS_PRE_SWEEP;
//
//    // for the series of instructions
//    static  MoveAction[4] current_instructions; //put int, should it be?
//    static int i_cur = 0;
//
//    switch (current_stage) {
//
//        case HS_PRE_SWEEP:
//            switch (do_move_action(&turn_pre_sweep))
//            {
//                // just a simple case of turning through an angle, then changing
//                // state.
//                case MA_COMPLETE:
//                    current_stage = HS_S_REPAIR;
//                    break;
//                default:
//                    break;
//            }
//            break;
//
//        case HS_S_RECHARGE:
//            /*
//                Searching for robots in need of recharging by sweeping left to right.
//
//                Most of this code is for dealing with the sweep, except checking
//                periodically for the presence of infrared.
//            */
//            switch (do_move_action(&sweep_l_to_r))
//            {
//                // just a simple case of turning through an angle, then changing
//                // state.
//                case MA_COMPLETE:
//                    current_stage = HS_S_REPAIR;
//                    break;
//                default:
//                    break;
//            }
//
//            if (looking_at() == R_RECHARGE) {
//                    update_position_given_motion_type(M_L_PIVOT_RIGHT);
//                    last_sweep = HS_S_RECHARGE;
//                    current_stage = HS_APPROACH;
//            }
//
//            break;
//
//        case HS_S_REPAIR:
//            /*
//                Searching for robots in need of repair by sweeping left to right.
//
//                Most of this code is for dealing with the sweep, except checking
//                periodically for the presence of infrared.
//            */
//            switch (do_move_action(&sweep_r_to_l))
//            {
//                // just a simple case of turning through an angle, then changing
//                // state.
//                case MA_COMPLETE:
//                    // get back on to the line then go home since there are
//                    // no robots left.
//                    // now get us back to the tunnel!
//
//                    actions_to_return_to_tunnel(current_instructions);
//                    i_cur = 0;
//                    current_stage = HS_HOMETIME;
//                    break;
//                default:
//                    break;
//            }
//
//
//            // now see if we're looking at a robot of the right sort. If so,
//            // go and get it.
//            if (looking_at() == R_REPAIR) {
//                    update_position_given_motion_type(M_L_PIVOT_LEFT);
//                    last_sweep = HS_S_REPAIR;
//                    current_stage = HS_APPROACH;
//             }
//
//             break;
//
//        case HS_APPROACH:
//            /*
//                If we've got there and grabbed it then change state to go home.
//                If we're not there yet but seem to be on course, keep going.
//                If we're off course, do something about it? (optionally).
//            */
//            switch (approach_and_grab())
//            {
//                case MA_COMPLETE:
//                    // prime with actions to do from here
//                    actions_to_return_to_tunnel(current_instructions);
//                    i_cur = 0;
//                    current_stage = HS_RETREIVE;
//                    break;
//            }
//            break;
//
//        case HS_RETREIVE:
//            // now get us back to the tunnel!
//
//            // the instructions to get back are stored in current_instructions
//            // before we switch to this phase
//            if (i_cur < 4) {
//                if (do_move_action(current_instructions[i_cur]) == MA_COMPLETE) {
//                    i_cur++;
//                }
//            }
//            else {
//                // We're back! reset this for next time.
//                current_stage = HS_PRE_SWEEP;
//                // and indicate that we're ready for line following to take charge
//                return RH_DROPOFF;
//            }
//        case HS_HOMETIME:
//            // the instructions to get back are stored in current_instructions
//            // before we switch to this phase
//            if (i_cur < 4) {
//                // work through the actions one by one.
//                if (do_move_action(current_instructions[i_cur]) == MA_COMPLETE) {
//                    i_cur++;
//                }
//            }
//            else {
//                // We're back!
//                // and indicate that we're ready for line following to take charge
//                return RH_DONE;
//        }
//    return RH_INPROGRESS;
//}
