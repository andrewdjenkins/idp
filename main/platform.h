/*
    platform.h
    
    Functions to get information from the sensors and control the motors and
    stuff.
*/

#ifndef PLATFORM_H_
#define PLATFORM_H_

#define LEFT 'L'
#define RIGHT 'R'

// all the set up needed for sensors etc.
void setup_platform(void);

/*
    INFRARED STUFF
*/

typedef enum MaybeRobotType
{
    R_REPAIR,
    R_RECHARGE,
    R_NONE,
    R_ERROR,
} MaybeRobotType;
/*
    Returns:
        R_RECHARGE if we're looking at a robot that needs charging.
        R_REPAIR if we're looking at a robot that needs repair.
        R_NONE if we're not looking at any robot.
        R_ERROR if we seem to be looking at a robot but can't work out what it
            is.
*/
MaybeRobotType looking_at(void);

// run the left motor at speed 'left' and the right motor at speed 'right'
void run_motor(int left, int right);

// activate the grabber.
void grabber_grab(void);

void grabber_release(void);

// ULTRASOUND STUFF
#define US_INVALID -1
// Returns the distance in cm as determined by the ultrasound sensor, or
// US_INVALID if no returning pulse is heard.
float ultrasound_range(void);

// start stop button
void start_button(void function());

// side is LEFT or RIGHT (i.e. 'L' or 'R') - returns True if the sensor on that
// side sees white.
bool line_white(char side);

#endif /* PLATFORM_H_ */
