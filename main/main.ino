
#include "field_motion.h"
#include "platform.h"

enum phase {
    FOLLOW_LINE_HOME_OUT,
    FIELD,
    FOLLOW_LINE_DROPOFF, // where we go to drop off the robot
    //FOLLOW_LINE_DROPOFF_OUT,
    FOLLOW_LINE_HOME,
};

// what are we doing at the moment?
enum phase current_phase;

void change_phase(enum phase new_phase) {
    current_phase = new_phase;
    if (new_phase == FIELD) {
        set_origin();
    } 
}

void setup() {
    setup_platform();
    setup_field_motion();
}

void loop() {
    
    switch (current_phase) {
        case FOLLOW_LINE_HOME_OUT:
            /*
                For going from home to the field - should end with a check if
                we're at the end of the line, and if we are call 
                change_phase(FIELD). Start this one by waiting for a button 
                press.
            */
            break;
            
        case FOLLOW_LINE_HOME:
            /*
                For going home from the field - the end of this is the end
                of the program, so just stop.
            */
            break;
            
        case FOLLOW_LINE_DROPOFF:
            /*
                Following line to dropoff, drop off a robot, then go back to the
                field. At the end of this we will ask it to release the grabber,
                then go back to the field and change phase to FIELD
            */
            break;
            
        case FIELD:
            /*
                code for operating in the field - look, get robot, go back to 
                the tunnel.
            */
            switch (look_for_robot()) {
                
                case RH_INPROGRESS:
                    // still going - do nothing.
                    break;
                case RH_DROPOFF:
                    change_phase(FOLLOW_LINE_DROPOFF);
                    break;
                case RH_DONE:
                    change_phase(FOLLOW_LINE_HOME);
                    break;
            }
            break;
    }
    
}
