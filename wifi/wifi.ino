#include <SPI.h>
#include <WiFiNINA.h>
#include <WiFiUdp.h>

#define W_MAX_CONNECT_TRIES 1

// wifi test
// the PLAN: connect to (something) and then create a print function that dumps
//info to it.

/*

 This example connects to an unencrypted Wifi network.
 Then it prints the  MAC address of the Wifi module,
 the IP address obtained, and other network details.

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */

char ssid[] = "CUED Robots";        // your network SSID (name)
char pass[] = "E5tnuLmTDtVbRfWREYQ.BivFYGAlHefu";    // your network password (use for WPA, or use as key for WEP)
int status = WL_IDLE_STATUS;     // the Wifi radio's status

// https://www.arduino.cc/en/Tutorial/WiFiNINAWiFiUdpSendReceiveString

unsigned int localPort = 2390;      // local port to listen on

char packetBuffer[255]; //buffer to hold incoming packet
char ReplyBuffer[] = "acknowledged"; // a string to send back

WiFiUDP Udp;

void setup() {
      Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
    Serial.println("Serial connected yo");
    int attempts = 0;
    
    // attempt to connect to Wifi network:
    while ((status != WL_CONNECTED) && (attempts++ < W_MAX_CONNECT_TRIES)) {
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
      	Serial.print("tryna connec to");
	Serial.println("ssid");
      	status = WiFi.begin(ssid, pass);

        // wait 10 seconds for connection:
        delay(10000);
    }
    Serial.println("Connected to wifi");
    printWifiStatus();
  
    // if you get a connection, report back via serial:
    Udp.begin(localPort);
}

void loop() {

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    IPAddress remoteIp = Udp.remoteIP();
    // read the packet into packetBufffer
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
      packetBuffer[len] = 0;
    }

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer);
    Udp.endPacket();
  }
}


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
