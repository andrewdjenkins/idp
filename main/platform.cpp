/*
    platform - contains all the functions for dealing with the physical robot.
    Namely:
        - Motors
        - Reflectivity sensors for line following
        - Ultrasound range sensor
        - Infrared robot sensor
        - Photo interruptor (for wheel encoder)
*/

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

#include "platform.h"

// PINOUT CONSTANTS

// Ultrasound pinout - should be digital-capable pins
#define US_TRIGGER_PIN 4
#define US_ECHO_PIN 5
// Safety LED pinout
#define LED_PIN 2
// Motor ports on Adafruit motor shield
#define LEFT_MOTOR_PORT 1
#define RIGHT_MOTOR_PORT 2
#define WINCH_MOTOR_PORT 3
// The analogue pin that the signal pin of the IR sensor is connected to
const int IR_AMPLITUDE_PIN = A2;
#define IR_5_PIN 1 // high if 5 pulses
#define IR_3_PIN 0 // high if 3 pulses
// The line-following sensor pins.
const int left_sensor_pin = A1;
const int right_sensor_pin = A0;
// start/stop button
const int BUTTON_PIN = A5;
bool btn_toggle = false;

// CONTROL CONSTANTS
#define WINCH_MOTOR_SPEED 255        // how fast do we run the motor?
#define WINCH_MOTOR_GRAB_MILLIS 2000 // how long do we run the motor for?
#define WINCH_MOTOR_RELEASE_MILLIS 1500

// Only allow setup_platform to be called once
bool platform_initialised = false;

/*
    MOTORS
*/

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(LEFT_MOTOR_PORT);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(RIGHT_MOTOR_PORT);
Adafruit_DCMotor *winchMotor = AFMS.getMotor(WINCH_MOTOR_PORT);

void setup_motor()
{
    // SETTING UP THE MOTOR
    AFMS.begin();
    // Set the speed to start, from 0 (off) to 255 (max speed)
    leftMotor->setSpeed(0);
    leftMotor->run(FORWARD);

    rightMotor->setSpeed(0);
    rightMotor->run(FORWARD);

    winchMotor->setSpeed(0);
    winchMotor->run(FORWARD);
}

void start_button(void function())
{
    //check button pressed, if so enter program condition (inside if statement)
    //HIGH means sensor is off, LOW means sensor is on
    //check button press here and if it is pressed then toggle run variable between true and false
    if (digitalRead(BUTTON_PIN) == 0) //functions based off of button pulling input pin HIGH
    {
        if (btn_toggle == false)
        {
            btn_toggle = true;
        }
        else
        {
            btn_toggle = false;
        }
    }

    if (btn_toggle == true)
    {
        function();
    }
    else
    {
        run_motor(0, 0);
    }
}

void flashing_led(bool state)
{
    if (state == 1)
    {
        unsigned long time = millis() % 1000;
        if (time < 500)
        {
            digitalWrite(LED_PIN, HIGH); // turn the LED on (HIGH is the voltage level)
        }
        else
        {
            digitalWrite(LED_PIN, LOW); // turn the LED off by making the voltage LOW
        }
    }
    else
    {
        digitalWrite(LED_PIN, LOW); // turn the LED off by making the voltage LOW
    }
}

void run_motor(int left, int right)
{
    // turn on the lamp if either motor is running.
    if (left != 0 && right != 0)
    {
        flashing_led(1);
    }
    else
    {
        flashing_led(0);
    }

    static int cur_speed_left = 0;
    static int cur_speed_right = 0;

    if (left != cur_speed_left)
    {

        if (left < 0)
        {
            leftMotor->setSpeed(-left);
            leftMotor->run(FORWARD);
        }
        else
        {
            leftMotor->setSpeed(left);
            leftMotor->run(BACKWARD);
        }
        cur_speed_left = left;
    }

    if (right != cur_speed_right)
    {
        if (right < 0)
        {
            rightMotor->setSpeed(-right);
            rightMotor->run(FORWARD);
        }
        else
        {
            rightMotor->setSpeed(right);
            rightMotor->run(BACKWARD);
        }
        cur_speed_right = right;
    }
}

void grabber_grab()
{
    winchMotor->setSpeed(WINCH_MOTOR_SPEED);
    winchMotor->run(FORWARD);
    delay(WINCH_MOTOR_GRAB_MILLIS);
    winchMotor->setSpeed(0);
}

void grabber_release()
{
    winchMotor->setSpeed(WINCH_MOTOR_SPEED);
    winchMotor->run(BACKWARD);
    delay(WINCH_MOTOR_RELEASE_MILLIS);
    winchMotor->setSpeed(0);
}

/*
    Reflectivity sensors for line following 
*/

#define IR_THRESHOLD_LEFT 200
#define IR_THRESHOLD_RIGHT 200

bool line_white(char sensor)
{

    int sensor_state;
    // select the relevant threshold to compare the input against
    int IR_threshold = (sensor == 'L') ? IR_THRESHOLD_LEFT : IR_THRESHOLD_RIGHT; 

    // if the reading of the left sensor is above a certain value, we assume that it is on the white line
    // if the reading of the right sensor is above a certain value, we assume that it is on the white line
    if (sensor == 'L')
    {
        sensor_state = analogRead(left_sensor_pin);
    }
    else if (sensor == 'R')
    {
        sensor_state = analogRead(right_sensor_pin);
    }
    else
    {
        Serial.println('sensor not defined');
    }

    if (sensor_state > IR_threshold)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/* 
    from ultrasound.ino
    based on datasheet at:
    https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf
*/

//TODO? Might want to make sure it can't be called more than once per reasonable
// time interval...

void setup_ultrasound()
{
    pinMode(US_ECHO_PIN, INPUT);
    pinMode(US_TRIGGER_PIN, OUTPUT);
}

int ultrasound_dt()
{
    // returns the time in microseconds between the pulse being sent and its
    // echo being recieved.
    int send_time = micros();
    // raise voltage on pin for 10 us
    digitalWrite(US_TRIGGER_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(US_TRIGGER_PIN, LOW);

    int dt;
    int high_start; // what time does the high pulse start?
    bool high_started = false;

    // 2 * 5 metres / 340 ms-1 = 29 ms so wait 30 ms before giving up.
    do
    {
        dt = micros() - send_time;
        if ((!high_started) && (digitalRead(US_ECHO_PIN) == HIGH))
        {
            // start timing when the echo pin rises
            high_start = micros();
            high_started = true;
        }
        if (high_started && (digitalRead(US_ECHO_PIN) == LOW))
        {
            // stop timing when it falls again.
            return micros() - high_start;
        }
    } while (dt < 35000);

    // if the pulse didn't get back then bin it.
    return (US_INVALID);
}

float ultrasound_range()
{
    // returns current range in centimetres
    int dt = ultrasound_dt();
    return (dt == US_INVALID) ? US_INVALID : (float)dt / 58;
}

/*
    INFRARED robot sensor and type detector
*/

#define IR_ROBOT_THRESHOLD 20 // if it falls below this then there is a robot present.

MaybeRobotType looking_at(void)
{
    // first see if the input value is below the threshold.
    float sensorValue = analogRead(IR_AMPLITUDE_PIN);

    // if it IS then there's a robot present.
    if (sensorValue <= IR_ROBOT_THRESHOLD)
    {
        if (digitalRead(IR_5_PIN) == HIGH && digitalRead(IR_3_PIN) == LOW)
        {
            return R_REPAIR;
        }
        else if (digitalRead(IR_3_PIN) == HIGH && digitalRead(IR_5_PIN) == LOW)
        {
            return R_RECHARGE;
        }
        else
        {
            return R_ERROR;
        }
    }
    return R_NONE;
}

void setup_IR()
{
    // IR_AMPLITUDE_PIN is analog so don't need pinmode.
    pinMode(IR_5_PIN, INPUT);
    pinMode(IR_3_PIN, INPUT);
}

// The OVERALL setup function to be called in setup()

void setup_platform()
{
    // this if is so that it can only be initialised once even if it's called
    // in multiple places
    if (!platform_initialised)
    {

        pinMode(LED_PIN, OUTPUT);
        pinMode(BUTTON_PIN, INPUT_PULLUP);
        setup_motor();
        setup_ultrasound();
        setup_IR();
        platform_initialised = true;
    }
}
