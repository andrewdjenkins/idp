#include <Wire.h>
#include <Adafruit_MotorShield.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

int buttonPin;
bool btn_toggle;
int ledPin = 0;

void setup()
{
    Serial.begin(9600); // set up Serial library at 9600 bps

    pinMode(ledPin, OUTPUT);

    AFMS.begin();
    leftMotor->setSpeed(150);
    leftMotor->run(BACKWARD);
    leftMotor->run(RELEASE);

    rightMotor->setSpeed(150);
    rightMotor->run(BACKWARD);
    rightMotor->run(RELEASE);

    btn_toggle = true; // boolean to toggle state of button, false = stop, true = start
    buttonPin = A5;    //whatever pin your button is plugged into
    pinMode(buttonPin, INPUT_PULLUP);
}

void run_motor(int LEFT, int RIGHT)
{
    leftMotor->setSpeed(LEFT);
    leftMotor->run(BACKWARD);
    rightMotor->setSpeed(RIGHT);
    rightMotor->run(BACKWARD);
    delay(10);

    unsigned long time = millis() % 1000;
    if (time < 500)
    {
        digitalWrite(ledPin, HIGH); // turn the LED on (HIGH is the voltage level)
    }
    else
    {
        digitalWrite(ledPin, LOW); // turn the LED off by making the voltage LOW
    }
}

void start_button()
{
    //check button pressed, if so enter program condition (inside if statement)
    //HIGH means sensor is off, LOW means sensor is on
    //check button press here and if it is pressed then toggle run variable between true and false
    if (digitalRead(buttonPin) == 0) //functions based off of button pulling input pin HIGH
    {
        if (btn_toggle == false)
        {
            btn_toggle = true;
        }
        else
        {
            btn_toggle = false;
        }
    }

    if (btn_toggle == true)
    {
        run_motor(100, 100);
    }
    else
    {
        run_motor(0, 0);
    }
}

void loop()
{
    start_button();
}