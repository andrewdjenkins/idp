/*
    motion_test.ino
    A test script for controlled free motion.
*/

#include "field_motion.h"
#include "platform.h"

#define US_TRIGGER_PIN 4
#define US_ECHO_PIN 5
#define US_INVALID -1

int amplitudePin = A2; //  The analogue pin that the signal pin of the sensor is connected to
int typePin5 = 1;
int typePin3 = 2;
int IRMax = 20;
float sensorValue;

// A pointer to the next instruction to be executed. Can be changed e.g. for
// 'emergency' avoiadance / stop actions.
MoveAction *i_ptr;

// define some movements.
MoveAction r_180 = {};

void setup()
{
    r_180.type = M_L_PIVOT_RIGHT;
    r_180.angle = 180;

    // we only have one thing we want to do so no need for an array of instructions
    i_ptr = &r_180;

    setup_platform();
    setup_field_motion();
}

void loop()
{
    // if the instruction isn't to nothing, then do the next instruction.
    if (i_ptr != NULL)
    {
        switch (do_move_action(i_ptr))
        {

        case MA_INPROGRESS:
            // if it's still in progress search for robots
            sensorValue = int(analogRead(amplitudePin));
            //Serial.println(sensorValue);

            if (sensorValue <= IRMax)
            {
                // stop now
                run_motor(0, 0);

                // and stop stuff being done next time
                i_ptr = NULL;

                Serial.println("Pointing at the robot");
                Serial.println("this is the distance to the robot:");
                Serial.println(ultrasound_range());
            }
            break;

        case MA_COMPLETE:
            i_ptr = NULL;
            break;

        case MA_ERROR:
            // if it's in the error state then stop the motors and indicate (HOW?) that there's an error.
            success = false;
            //motor_left(0);
            //motor_right(0);
        }
    }
}
