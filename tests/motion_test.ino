/*
    motion_test.ino
    A test script for controlled free motion.
*/  

#include "field_motion.h"
#include "platform.h"

// A pointer to the next instruction to be executed. Can be changed e.g. for 
// 'emergency' avoiadance / stop actions.
MoveAction* i_ptr;
// number of scheduled instructions
int n_instructions;
// index of current scheduled instruction.
int cur_instruction = 0;
// list of pointers to scheduled instructions
MoveAction* instructions[10];
// did the last action complete in the last loop?
bool success = false;

// define some movements.
MoveAction forward_50 = {};
MoveAction r_180 = {};    

void setup ()
{
    forward_50.type = M_FORWARD;
    forward_50.distance = 50;
    
    r_180.type = M_L_PIVOT_RIGHT;
    r_180.angle = 180;
    
    setup_platform();
    instructions[0] = &forward_50;
    instructions[1] = &r_180;
    instructions[2] = &forward_50;

}

void loop() {   
    // PLACE ANY CHECKS HERE - can play with the instruction if need be.
    
    // if there are instructions not yet done, then do them.
    if (cur_instruction < n_instructions) {
        // the action which is currently being undertaken.
        i_ptr = instructions[cur_instruction];
    } else i_ptr = NULL;
    
    // if the instruction isn't to nothing, then do the next instruction.
    if (i_ptr != NULL) {
        switch (do_move_action(i_ptr)) {

            case MA_INPROGRESS:
                // if it's still in progress let the loop continue
                success = false;
                break;

            case MA_COMPLETE:
                // if it's completed the current instruction then increment the instruction cursor.
                success = true;
                break;
                
            case MA_ERROR:
                // if it's in the error state then stop the motors and indicate (HOW?) that there's an error.
                success = false;
                //motor_left(0);
                //motor_right(0);
        }
    }
    if (success) cur_instruction++;
}
