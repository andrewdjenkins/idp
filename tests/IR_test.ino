#define US_TRIGGER_PIN 4
#define US_ECHO_PIN 5
#define US_INVALID -1

int amplitudePin = A2; //  The digital pin that the signal pin of the sensor is connected to
int typePin5 = 1;
int typePin3 = 2;
int IRMax = 20;
float sensorValue;

void setup_ultrasound()
{
    pinMode(US_ECHO_PIN, INPUT);
    pinMode(US_TRIGGER_PIN, OUTPUT);
}

int ultrasound_dt()
{
    // returns the time in microseconds between the pulse being sent and its
    // echo being recieved.
    int send_time = micros();
    // raise voltage on pin for 10 us
    digitalWrite(US_TRIGGER_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(US_TRIGGER_PIN, LOW);

    int dt;
    int high_start; // what time does the high pulse start?
    bool high_started = false;

    // 2 * 5 metres / 340 ms-1 = 29 ms so wait 30 ms before saying sod it
    do
    {
        dt = micros() - send_time;
        if ((!high_started) && (digitalRead(US_ECHO_PIN) == HIGH))
        {
            // start timing when the echo pin rises
            high_start = micros();
            high_started = true;
        }
        if (high_started && (digitalRead(US_ECHO_PIN) == LOW))
        {
            // stop timing when it falls again.
            return micros() - high_start;
        }
    } while (dt < 35000);

    // if the pulse didn't get back then bin it.
    return (US_INVALID);
}

float ultrasound_range()
{
    // returns current range in centimetres
    int dt = ultrasound_dt();
    return (dt == US_INVALID) ? US_INVALID : (float)dt / 58;
}

void setup()
{
    Serial.begin(9600); //  Setup serial port to send key codes to computer
    setup_ultrasound();
}

void loop()
{
    sensorValue = int(analogRead(amplitudePin));
    //Serial.println(sensorValue);

    if (sensorValue <= IRMax)
    {
        Serial.println("Pointing at the robot");
        Serial.println("this is the distance to the robot:");
        Serial.println(ultrasound_range());
        if (digitalRead(typePin5) == 1 && digitalRead(typePin3) == 0)
        {
            Serial.println("This robot requires service");
        }
        else if (digitalRead(typePin3) == 1 && digitalRead(typePin5) == 0)
        {
            Serial.println("This robot requires charging");
        }
        else
        {
            Serial.println("Type of robot unknown");
        }
    }
}
