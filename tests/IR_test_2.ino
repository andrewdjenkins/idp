/*
    IR_test_2
*/

#include "platform.h"

void setup() {
    Serial.begin(9600);
    setup_platform();
}

void loop() {

    MaybeRobotType robot = looking_at();
    
    if (robot != R_NONE)  // i.e. if we are actually looking at a robot
    {
        Serial.println("Pointing at the robot");
        Serial.println("this is the distance to the robot:");
        Serial.println(ultrasound_range());
        if (robot == R_REPAIR)
        {
            Serial.println("This robot requires service");
        }
        else if (robot == R_RECHARGE)
        {
            Serial.println("This robot requires charging");
        }
        else {
            Serial.println("R_ERROR - both pins either high or low.");
        }
    }
}
