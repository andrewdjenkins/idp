#include <Wire.h>
#include <Adafruit_MotorShield.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

void setup()
{
    Serial.begin(9600);
    AFMS.begin();
    leftMotor->setSpeed(255);
    leftMotor->run(BACKWARD);
    leftMotor->run(RELEASE);

    rightMotor->setSpeed(255);
    rightMotor->run(BACKWARD);
    rightMotor->run(RELEASE);
}

void forward(int distance)
{
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
    int time0 = floor(distance * 5 / 80);
    delay(time0 * 1000);
}

void turn_right(int degrees)
{
    //rotating clockwise
    rightMotor->run(FORWARD);
    leftMotor->run(RELEASE);
    int time1 = floor(degrees * 40 / (4 * 360));
    delay(time1 * 1000);
}

void turn_left(int degrees)
{
    //rotating anticlockwise
    leftMotor->run(FORWARD);
    rightMotor->run(RELEASE);
    int time2 = floor(degrees * 40 / (4 * 360));
    delay(time2 * 1000);
}

void loop()
{

    forward(60);
    turn_right(90);
    forward(120);
    leftMotor->run(RELEASE);
    rightMotor->run(RELEASE);
    turn_left(90);
    turn_right(180);
    leftMotor->run(RELEASE);
    rightMotor->run(RELEASE);
    delay(2000);
    turn_right(90);
    forward(90);
}