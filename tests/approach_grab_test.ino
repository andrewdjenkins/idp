/*
    Approach & Grab test.
*/

#include "field_motion.h"
#include "platform.h"

void setup() {
    setup_platform();
    setup_field_motion();
    Serial.begin(9600);
}

bool done = false;

void loop() {
    if (!done) {
        switch (approach_and_grab())
        {
        case MA_INPROGRESS:
            break;
        
        case MA_ERROR:
            Serial.println("I might be off course.");
            break;
            
        case MA_COMPLETE:
            Serial.println("I think I'm done.");
            done = true;
        }
    }
}
