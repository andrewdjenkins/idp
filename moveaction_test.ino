#include "field_motion.h"
#include "platform.h"

MoveAction actions[4];
int i = 0;
int n_actions = 4;
 
void setup()
{
    setup_field_motion();
    setup_platform();
    
    actions[0].type = M_FORWARD;
    actions[0].distance = 50;
    actions[1].type = M_L_PIVOT_LEFT;
    actions[1].angle = 360;
    actions[2].type = M_L_PIVOT_RIGHT;
    actions[2].angle = 360;
    actions[3].type = M_BACKWARD;
    actions[3].distance = 50;
}

void main() {
    if (i < n_actions) {
        if (do_move_action(&actions[i]) == MA_COMPLETE) {
            i++;
        }
    }    
}
