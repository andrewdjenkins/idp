#!/usr/bin/env python3
# contact_and_listen.py - Andrew Jenkins 2020 for IDP
# First of all, ping the robot to let it know our IP
# thereafter, display any messages that reach us.

import socket

addr = "10.69.212.22"
port = 2390
localport = 6969
secret = b"hello"

# port to listen on
s2 = socket.socket(type=socket.SOCK_DGRAM)
s2.bind(("", localport))

# send the ping
s1 = socket.socket(type=socket.SOCK_DGRAM)
s1.sendto(secret, (addr, port))
print(f"Sent UDP signal to {addr}:{port}")
print(f"Listening on {localport}") 
# now listen to what the robot has to say
while True:
    print(str(s2.recv(1024), encoding="ascii"))
