/*
    photointerrupt_test.ino
    Dead simple program to increment a counter when the respective interrupt
    pins go high, accompanied by slightly less dead-simple logic.
    
    36 teeth => 36 rising and 36 falling edges
             => each falling edge corresponds to exactly 10 degrees
                (each change corresponds to roughly 5 degrees)
*/  

// the pins for the interrupts
#define INT_LEFT_PIN 6
#define INT_RIGHT_PIN 7

#define WHEEL_CIRCUMFERENCE_CM 30
#define DEGREES_PER_INTERRUPT 10
#define WHEELSPAN_CM 40

enum MotionState {
    MS_STOPPED,
    // these are for turning with both wheels at different speeds.
    MS_LEFT,
    MS_RIGHT,
    // these are for ostensibly straight motion
    MS_FORWARD,
    MS_BACKWARD,
    // these are for single-encoder operation: rotation with LH wheel stationary
    MS_L_PIVOT_LEFT,
    MS_L_PIVOT_RIGHT,
};

volatile unsigned int left_count = 0;
volatile unsigned int right_count = 0;

// to be set by the motor control routines based on rel. motion of wheels.
MotionState motion_state;

// X, Y, Theta
typedef struct XYT {
    float theta;
    float x;
    float y;
} XYT;

// where angle is the angle relative to the y axis. (i.e. to an axis _|_ to x)
float angle = 0;
float x = 0;
float y = 0;

// increment the position state variables based on the position change vector
void inc_position(XYT d_pos) {
    angle += d_pos.theta;
    x += d_pos.x;
    y += d_pos.y;
}

// update the position state variables based on the current and previous 
void update_position() {
    // these are the v
    static int x_count_last = 0;
    static int y_count_last = 0;
}

// update the position of the centre of the axle based on an advance where
// both wheels move straight ahead, and the wheel rotates n counts
XYT forward_dpos(int theta_0, int n_right)
{
    // arc length traced by rh wheel
    float ds = (n_right * DEGREES_PER_INTERRUPT / 360)
                * WHEEL_CIRCUMFERENCE_CM;
    
    float dy = ds * sin (theta);
    float dx = ds * cos (theta);
    XYT p; // encapsulate all of the deltas into a struct
    
    p.theta = 0;
    p.x = dx;
    p.y = dy;
    
    // this can be used to update the position state variables
    return p;
}

// return the change in position and rotation of the centre of the axle based 
// on a rotation where the left wheel is fixed and the right wheel rotates n 
// counts.
XYT l_pivot_dpos(float theta_0, int n_right)
{
    // arc length traced by rh wheel rel to lh wheel
    float ds = (n_right * DEGREES_PER_INTERRUPT / 360)
                * WHEEL_CIRCUMFERENCE_CM;
                
    // change in abs angle of robot
    float d_theta = ds / WHEELSPAN_CM;
    float avg_theta = theta_0 + 0.5 * d_theta; 
    float dy = ds * sin (avg_theta);
    float dx = ds * cos (avg_theta);
    XYT p; // encapsulate all of the deltas into a struct
    
    p.theta = d_theta;
    p.x = dx;
    p.y = dy;
    
    // this can be used to update the position state variables
    return p;
}

void ISR_int_left () {
    // called when the left hand side changes
    left_count++;
}

void ISR_int_right () {
    // called when right hand side changes
    right_count++;
}

void setup_interrupts() {
    // reasonable options are CHANGE / RISING / FALLING
    attachInterrupt(digitalPinToInterrupt(INT_LEFT_PIN), ISR_int_left, FALLING);
    attachInterrupt(digitalPinToInterrupt(INT_RIGHT_PIN), ISR_int_right, FALLING);
}

void setup ()
{
    setup_interrupts();
    Serial.begin(9600);
}

void loop(){
    Serial.print("Left: ");
    Serial.print(left_count);
    Serial.print(" Right: ");
    Serial.println(right_count);
    delay(0.1);
}
